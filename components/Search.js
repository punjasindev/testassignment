import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';

export const SearchInput = ({value, onChange, style, onPress}) => {
  return (
    <TextInput
      value={value}
      onChangeText={onChange}
      placeholder={'Search'}
      style={style}
    />
  );
};

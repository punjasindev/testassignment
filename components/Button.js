import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

export const Button = ({title, onPress, style, textStyle}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          borderWidth: 1,
          borderRadius: 15,
          height: 50,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          ...style,
        }}>
        <Text style={textStyle}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

import React from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';

export const Restaurant = ({image, title, description}) => {
  console.log({image});
  return (
    <View
      style={{
        height: 100,
        width: '100%',
        borderWidth: 1,
        marginBottom: 15,
        flexDirection: 'row',
      }}>
      <Image
        resizeMethod={'contain'}
        style={{
          height: '100%',
          width: 100,
        }}
        source={{
          uri: image,
        }}
      />
      <View style={{flex: 1}}>
        <Text ellipsizeMode={'tail'}>{title}</Text>
        <Text ellipsizeMode={'tail'}>{description}</Text>
      </View>
    </View>
  );
};

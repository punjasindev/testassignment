import React, {useEffect, useState} from 'react';
import {ScrollView, View} from 'react-native';
import {SearchInput} from '../components/Search';
import axios from 'axios';
import {useAsyncStorage} from '../utill';
import {Restaurant} from '../components/Restaurant';

export default function Search() {
  const [keyword, setKeyword] = useState('');
  const [food, setFoods] = useAsyncStorage('shop', []);
  useEffect(() => {
    if (keyword !== '') {
      axios
        .get(
          `https://maps.googleapis.com/maps/api/place/nearbysearch/json?input=${keyword}&location=13.893983,100.5141033&language=th&radius=1500&type=restaurant&key=AIzaSyDalP8MVHmVXidKmb0hB6NuKaWx4hth2tQ`,
        )
        .then(res => {
          console.log({res});
          setFoods(res.data.results);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        paddingHorizontal: 15,
      }}>
      <SearchInput
        value={keyword}
        onChange={setKeyword}
        style={{
          margin: 15,
          width: '100%',
          height: 40,
          borderWidth: 1,
          borderColor: 'black',
          borderRadius: 5,
          paddingHorizontal: 15,
          textAlign: 'center',
        }}
      />
      <ScrollView
        style={{
          width: '100%',
        }}>
        {food?.map(item => {
          return (
            <Restaurant
              title={item.name}
              description={item.vicinity}
              image={item.icon}
            />
          );
        })}
      </ScrollView>
    </View>
  );
}

import React from 'react';
import {View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

export default function Map() {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
      }}>
      <MapView
        style={{
          width: '100%',
          height: '100%',
        }}>
        <Marker
          coordinate={{latitude: '13.893983', longitude: '100.5141033'}}
          title={'JENOSIZE'}
        />
      </MapView>
    </View>
  );
}

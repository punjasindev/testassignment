import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import Logo from '../assets/logo.svg';
import {Button} from '../components/Button';
import {useNavigation} from '@react-navigation/native';
function HomeScreen() {
  const router = useNavigation();

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
      }}>
      <Logo width={200} height={200} />

      <View
        style={{
          width: '100%',
          paddingHorizontal: 15,
          alignItems: 'center',
        }}>
        <Button
          style={{
            width: 300,
            marginBottom: 15,
            backgroundColor: 'gray',
          }}
          onPress={() => {
            router.navigate('Search');
          }}
          textStyle={{
            fontSize: 20,
            fontWeight: 'bold',
            color: 'white',
          }}
          title={'ค้นหาร้านอาหาร'}
        />
        <Button
          style={{
            width: 300,
            backgroundColor: 'gray',
          }}
          onPress={() => {
            router.navigate('Map');
          }}
          textStyle={{
            fontSize: 20,
            fontWeight: 'bold',
            color: 'white',
          }}
          title={'แผนที่บริษัท Jenosize'}
        />
      </View>
    </View>
  );
}
export default HomeScreen;
